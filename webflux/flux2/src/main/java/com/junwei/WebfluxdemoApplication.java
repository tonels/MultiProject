package com.junwei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

/**
 * webflux 非阻塞 运行在netty
 * 不能使用基于jdbc的数据库
 * 垂直扩展
 *
 * 异步servlet线程可以马上进行返回去处理其它请求
 * 同步servlet阻塞线程
 * 对于浏览器来说都是同步操作
 */
@SpringBootApplication
@EnableReactiveMongoRepositories
public class WebfluxdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxdemoApplication.class, args);
    }

}
