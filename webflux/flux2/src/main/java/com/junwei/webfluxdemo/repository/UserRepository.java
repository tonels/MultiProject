package com.junwei.webfluxdemo.repository;

import com.junwei.webfluxdemo.entity.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * @ClassName UserRepository
 * @Description TODO
 * @Author 王军伟
 * @Date 2020/2/24 下午8:56
 */
@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {

    Flux<User> findByAgeBetween(int start, int end);

    @Query("{'age':{'$gte':20,'$lte':30}}")
    Flux<User> oldUser();

}
