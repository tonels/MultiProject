package com.junwei.webfluxdemo.util;

import com.junwei.webfluxdemo.exceptions.CheckException;

import java.util.stream.Stream;

/**
 * @ClassName CheckUtil
 * @Description TODO
 * @Author 王军伟
 * @Date 2020/2/25 下午3:08
 */
public class CheckUtil {

    private static final String[] INVALID_NAMES = {"admin", "管理员"};

    public static void checkName(String value) {
        Stream.of(INVALID_NAMES)
                .filter(name -> name.equalsIgnoreCase(value))
                .findAny().ifPresent(name -> {
            throw new CheckException("name", value);
        });
    }
}
