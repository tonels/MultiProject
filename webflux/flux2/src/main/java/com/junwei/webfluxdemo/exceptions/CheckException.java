package com.junwei.webfluxdemo.exceptions;

import lombok.Data;

/**
 * @ClassName CheckException
 * @Description TODO
 * @Author 王军伟
 * @Date 2020/2/25 下午3:09
 */
@Data
public class CheckException extends RuntimeException {
    private static final long serialVersionUID = 3974700361560778140L;

    private String fieldName;

    private String fieldValue;

    public CheckException(String fieldName, String fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public CheckException() {
    }

    public CheckException(String s) {
        super(s);
    }

    public CheckException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CheckException(Throwable throwable) {
        super(throwable);
    }

    public CheckException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
