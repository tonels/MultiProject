package com.junwei.webfluxdemo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @ClassName User
 * @Description TODO
 * @Author 王军伟
 * @Date 2020/2/24 下午8:47
 */
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 457038306661693774L;

    @Id
    @ApiModelProperty(value = "用户id")
    private String id;
    @NotBlank
    @ApiModelProperty(value = "用户姓名")
    private String name;
    @Range(min = 10, max = 100)
    @ApiModelProperty(value = "用户年龄")
    private int age;

}
