package com.media;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 封装多媒体文件解析
 */
public class VideoInfo {

    private List<String> pathRight = new ArrayList<String>();// 经处理后，得到的有效地址
    private List<String> path1 = new ArrayList<String>();// ffmpeg能处理的文件的地址集
    private List<String> path2 = new ArrayList<String>();// ffmpeg不能处理的文件的地址集
    public static int fileNum = 0;// 所有经过验证合法，并需要转换的视频数量

    private String mencoder_home = "E:/mediaCode/mplayer-svn-38151-x86_64/mencoder.exe";//mencoder.exe所放的路径
    private String mplay_home = "E:/mediaCode/mplayer-svn-38151-x86_64/mplayer.exe";//mplayer.exe所放的路径
    private String ffmpeg_home = "D:/Tools/ffmpeg/bin/ffmpeg.exe";//ffmpeg.exe所放的路径

    private String output = "E:/mediaCode/output/";//转换后的flv文件所放的文件夹位置
    private String temp = "E:/mediaCode/output/temp/";//存放rm,rmvb等无法使用ffmpeg直接转换为flv文件先转成的avi文件
    private String img = "E:/mediaCode/output/img/";//转换过程中产生的缩略图

    public static void main(String[] args) {
        VideoInfo videoInfo = new VideoInfo();
        File file = FileUtil.file("E:\\mediaCode\\input\\f4.rmvb");
        // 截图测试
        // boolean b = videoInfo.processImg(file.getAbsolutePath());
        int videoTime = videoInfo.getVideoTime(file.getAbsolutePath());


    }

    /**
     * ffmpeg---截图
     * @param str
     * @return
     */
    public boolean processImg(String str) {
        List<String> commend = new java.util.ArrayList<String>();
        commend.add("\"" + ffmpeg_home + "\"");
        commend.add("-i");
        commend.add("\"" + str + "\"");
        commend.add("-y");
        commend.add("-f");
        commend.add("image2");
        commend.add("-ss");
        commend.add("5");
        commend.add("-t");
        commend.add("0.001");
        commend.add("-s");
        commend.add("320x240");
        commend.add("\"" + img +  "1.jpg\"");
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commend);
            Process start = builder.start();
            System.out.println("JSONUtil.toJsonStr(start) = " + JSONUtil.toJsonStr(start));

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 获取视频总时间
     * @param videoPath    视频路径
     * @return
     */
    public int getVideoTime(String videoPath) {
        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpeg_home);
        commands.add("-i");
        commands.add(videoPath);
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            final Process p = builder.start();

            //从输入流中读取视频信息
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();

            //从视频信息中解析时长
            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
            Pattern pattern = Pattern.compile(regexDuration);
            Matcher m = pattern.matcher(sb.toString());
            if (m.find()) {
                int time = getTimelen(m.group(1));
                System.out.println(videoPath + ",视频时长：" + time + ", 开始时间：" + m.group(2) + ",比特率：" + m.group(3) + "kb/s");
                return time;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    //格式:"00:00:10.68"
    private int getTimelen(String timelen) {
        int min = 0;
        String[] strs = timelen.split(":");
        if (strs[0].compareTo("0") > 0) {
            min += Integer.parseInt(strs[0]) * 60 * 60;//秒
        }
        if (strs[1].compareTo("0") > 0) {
            min += Integer.parseInt(strs[1]) * 60;
        }
        if (strs[2].compareTo("0") > 0) {
            min += Math.round(Float.parseFloat(strs[2]));
        }
        return min;
    }




}