package com.redisson.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfig {

//    @Bean
//    public RedissonClient redisson() throws IOException {
//
//        // 两种读取方式，Config.fromYAML 和 Config.fromJSON
////        Config config = Config.fromJSON(RedissonConfig.class.getClassLoader().getResource("redisson-config.json"));
//        Config config = Config.fromYAML(RedissonConfig.class.getClassLoader().getResource("redisson-config.yaml"));
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        System.out.println(objectMapper.writeValueAsString(config));
//
//
////        RedissonClient redisson = Redisson.create();
//
//        return Redisson.create(config);
//    }

    @Bean
    public RedissonClient client() {
        Config config = new Config();

        // 设置序列化
//        config.setCodec(new FstCodec())
//        JsonJacksonCodec codec = new JsonJacksonCodec();
//        JsonJacksonCodec codec = new SmileJacksonCodec();
//        StringCodec codec = new StringCodec();

        CustomerCodec codec = new CustomerCodec();
        config.setCodec(codec)
                .useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://localhost:6379")
                .setDatabase(15);

        //默认连接上 127.0.0.1:6379
        RedissonClient redisson = Redisson.create(config);
        return redisson;
    }

}
