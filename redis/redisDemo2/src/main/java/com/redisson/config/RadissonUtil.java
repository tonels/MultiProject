package com.redisson.config;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.redisson.api.RBucket;
import org.redisson.api.RKeys;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
public class RadissonUtil {


    @Resource
    private RedissonClient redissonClient;

    /**
     * 返回当前库
     * 所有的Key
     *
     * @param pattern
     * @return
     */
    public List<String> findKeysByPattern(String pattern) {
        RKeys keys = redissonClient.getKeys();
        Iterable<String> keysByPattern = keys.getKeysByPattern(pattern);
        return Lists.newArrayList(keysByPattern);
    }

//    -*-*-*-*-*-*-*-*String 类型操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    /**
     * 一般设置
     *
     * @param key
     * @param value
     * @param <T>
     */
    public <T> void set(String key, T value) {
        RBucket<Object> keyObj = redissonClient.getBucket(key);
        keyObj.set(value);
    }

    /**
     * 设置过期，默认秒
     *
     * @param key
     * @param value
     * @param activeTime
     * @param <T>
     */
    public <T> void setEx(String key, T value, long activeTime) {
        RBucket<String> keyObj = redissonClient.getBucket(key);
        keyObj.set(JSON.toJSONString(value), activeTime, TimeUnit.SECONDS);
    }

    /**
     * todo 偏移量 待补充
     *
     * @param key
     * @param value
     * @param activeTime
     * @param <T>
     */
    public <T> void setRange(String key, T value, long activeTime) {

    }

    /**
     * 获取指定 key 的值
     *
     * @param key
     * @return
     */
    public Object get(String key) {
        return redissonClient.getBucket(key).get();
    }

    /**
     * 获取指定 key 的值
     *
     * @param key
     * @return
     */
    public <T> T getObject(String key, Class<T> targetClass) {
        Object o = redissonClient.getBucket(key).get();
        return JSON.parseObject(o.toString(), targetClass);
    }

//    -*-*-*-*-*-*-*-*Hash 类型操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    /**
     * 获取存储在哈希表中指定字段的值
     *
     * @param hashKey
     * @param field
     * @return
     */
    public <T> T hGet(String hashKey, String field, Class<T> targetClass) {
        RMap<String, Object> map = redissonClient.getMap(hashKey);
        return JSON.parseObject(map.get(field).toString(), targetClass);
    }

    /**
     * @param hashKey
     * @param field
     * @return
     */
    public void hSet(String hashKey, String field, Object value) {
        RMap<String, Object> map = redissonClient.getMap(hashKey);
        map.put(field, value);
    }

    /**
     * 获取所有给定字段的值
     *
     * @param hashKey
     * @return
     */
    public Map<String, Object> hGetAll(String hashKey) {
        RMap<String, Object> map = redissonClient.getMap(hashKey);
        return map.readAllMap();
    }

//    -*-*-*-*-*-*-*-*Zset 类型操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

//    -*-*-*-*-*-*-*-*List 类型操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//    -*-*-*-*-*-*-*-*Set 类型操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

//    -*-*-*-*-*-*-*-* 分布式锁-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//    -*-*-*-*-*-*-*-* 队列操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//    -*-*-*-*-*-*-*-* MQ操作-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*


}
