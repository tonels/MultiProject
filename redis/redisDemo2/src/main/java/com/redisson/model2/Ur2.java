package com.redisson.model2;

import lombok.Data;

import java.io.Serializable;

@Data
public class Ur2 implements Serializable {
    private Long id;
    private String name;

    @Override
    public String toString() {
        return "Ur2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
