package com.redisson.model;

import lombok.Data;

import java.io.Serializable;
import java.util.StringJoiner;

@Data
public class Ur implements Serializable {
    private Long id;
    private String name;

    @Override
    public String toString() {
        return new StringJoiner(", ", Ur.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString();
    }
}
