package com.redisson.controller;

import com.redisson.config.RadissonUtil;
import com.redisson.config.RedisUtil;
import com.redisson.model.Ur;
import com.redisson.service.RedissLockUtil;
import jodd.util.MathUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.IntStream;

/**
 * 这里只测试序列化对象
 */
@RestController
@RequestMapping("/base")
@Slf4j
public class BaseTestController {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RedissonClient redissonClient;


    // -*-*-*RestTemplate-Util-*-*-*-*-*-*-*-*-RestTemplate-Util*-*-*-*-*-*-*-*-*-*-*-*-RestTemplate-Util-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    /**
     * localhost:8080/base/t1/ls/util
     * localhost:8080/base/t1/ls/radisson
     *
     * @param key
     * @param type
     * @return
     */
    @GetMapping("/t1/{key}/{type}")
    public Object t1(@PathVariable String key,
                     @PathVariable String type) {
        if (type.equals("util")) {
            Ur ur = new Ur();
            ur.setId(MathUtil.randomLong(1, 20));
            ur.setName("util_" + key);
            redisUtil.set(ur.getName(), ur);
            Ur object = redisUtil.getObject(ur.getName(), Ur.class);
            return object;
        } else if (type.equals("radisson")) {
            Ur ur = new Ur();
            ur.setId(MathUtil.randomLong(1, 20));
            ur.setName("radisson_" + key);
            // 存放 Hash
            RMap<String, Ur> ss = redissonClient.getMap("UR");
            ss.put(ur.getName(), ur);
            return redissonClient.getMap("UR").get(ur.getName());
        }
        return null;

    }

    // -*-*-*Radisson*-*-*-*-*-*-*-*-Radisson-*-*-*-*-*-*-*-*-*-*-*-Radisson*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    @Autowired
    RadissonUtil radissonUtil;

    /**
     * todo 这里只补充了几个方法，后面待补充
     * localhost:8080/base/t2/hhas
     *
     * @param key
     */
    @GetMapping("/t2/{key}")
    public void t2(@PathVariable String key) {
        log.info("所有Key:{}", radissonUtil.findKeysByPattern(null));

        Ur ur = new Ur();
        ur.setId(MathUtil.randomLong(1, 20));
        ur.setName("t2_" + key);
        radissonUtil.set(ur.getName(), ur);
        log.info("set测试成功:{}", radissonUtil.getObject(ur.getName(), Ur.class));

        Ur hur = new Ur();
        hur.setId(MathUtil.randomLong(1, 20));
        hur.setName("hash_" + key);
        radissonUtil.hSet("HH", hur.getName(), hur);
        log.info("Hash测试成功:{}", radissonUtil.hGet("HH", hur.getName(), Ur.class));
    }




}
