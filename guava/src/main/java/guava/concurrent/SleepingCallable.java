package guava.concurrent;

import java.util.concurrent.Callable;

/**
 *
 */
public class SleepingCallable implements Callable<String> {

    private String response;
    private long sleep;

    public SleepingCallable(String response, long sleep) {
        this.response = response;
        this.sleep = sleep;
    }

    public String call() throws Exception {
        System.out.println(Thread.currentThread().getName() + ": 自定义线程启动");
        Thread.sleep(sleep);
        System.out.println(Thread.currentThread().getName() + ": 自定义线程结束");
        return response;
    }
}
