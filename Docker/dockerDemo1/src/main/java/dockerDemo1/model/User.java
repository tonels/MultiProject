package dockerDemo1.model;

import lombok.AllArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
public class User{
    int age;
    String name;
}
