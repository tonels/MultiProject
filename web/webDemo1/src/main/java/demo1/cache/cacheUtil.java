package demo1.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.poi.ss.formula.functions.T;

import java.util.concurrent.TimeUnit;

public class cacheUtil {
    public static LoadingCache<String, T> get1(String empId, T t) {
        LoadingCache<String, T> build = CacheBuilder.newBuilder()
                .maximumSize(100)                             // maximum 100 records can be cached
                .expireAfterAccess(1, TimeUnit.MINUTES)      // cache will expire after 30 minutes of access
                .build(new CacheLoader<String, T>() {  // build the cacheloader
                    @Override
                    public T load(String empId) throws Exception {
                        //make the expensive call
                        return t;
                    }
                });
        return build;
    }
}
