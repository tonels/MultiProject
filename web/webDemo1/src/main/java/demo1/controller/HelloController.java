package demo1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/")
public class HelloController {

    @GetMapping("/index")
    public ModelAndView index(){
        return new ModelAndView("index.html");
    }

    @GetMapping("/index2")
    public String index2(){
        return "index";
    }

    @GetMapping("/home")
    public ModelAndView home(){
        System.out.println("进入方法");

        return new ModelAndView("home.html");
    }

    @GetMapping("/cookies")
    public void cookies(HttpServletResponse response, HttpServletRequest request) {

        Cookie cookie = new Cookie("looo", "looo");
        Cookie freshCookie = new Cookie("lplp", "lplp");

        cookieFormat(request, cookie);
        cookieFormat(request, freshCookie);

        response.addCookie(cookie);
        response.addCookie(freshCookie);
    }
    private void cookieFormat(HttpServletRequest request, Cookie cookie) {
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        //设置过期时间4小时
        cookie.setMaxAge(4 * 60 * 1000);
        cookie.setSecure(request.isSecure());
        cookie.setDomain(request.getServerName().toLowerCase());
    }


   @GetMapping("/header")
    public void header(HttpServletResponse response, HttpServletRequest request){
       response.addHeader("header1", "header1");
       response.addHeader("header2", "header2");
    }


}
