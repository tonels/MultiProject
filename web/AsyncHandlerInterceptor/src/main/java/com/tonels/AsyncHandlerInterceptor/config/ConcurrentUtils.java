package com.tonels.AsyncHandlerInterceptor.config;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ConcurrentUtils {

//    public static void stop(ExecutorService executor) {
//        try {
//            executor.shutdown();
//            executor.awaitTermination(60, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            System.err.println("termination interrupted");
//        } finally {
//            if (!executor.isTerminated()) {
//                System.err.println("killing non-finished tasks");
//            }
//            executor.shutdownNow();
//        }
//    }

    public static void stop(ExecutorService executor) {
        if (executor != null && !executor.isShutdown()) {
            executor.shutdown();
            try {
                while (!executor.awaitTermination(2, TimeUnit.SECONDS)) {
                    log.debug("wait for the thread pool task to end.");
                }
            } catch (InterruptedException e) {
                log.error("close pool is error!", e);
                Thread.currentThread().interrupt();
            }
        }
    }


    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String CurrentThreadName() {
        return Thread.currentThread().getName();
    }

}
