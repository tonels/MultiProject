package com.tonels;

import com.tonels.util.DateUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

/**
 * 批量修改文件日期
 */
public class TimeEdit {

    public static final String PATH_Target = "\\\\192.168.1.74\\public\\usr\\huizong\\06\\01";

    public static void main(String[] args) throws IOException {

        Path startingDir = Paths.get(PATH_Target);
        List<Path> result = new LinkedList<Path>();
        Files.walkFileTree(startingDir, new FindJavaVisitor(result));
        System.out.println("result.size()=" + result.size());
    }

    private static class FindJavaVisitor extends SimpleFileVisitor<Path> {
        private List<Path> result;

        public FindJavaVisitor(List<Path> result) {
            this.result = result;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if (file.toString().endsWith(".jpg")) {
                file.toFile().setLastModified(jpgTime());
            } else if (file.toString().endsWith(".xml")) {
                file.toFile().setLastModified(xmlTime());
            }
            return FileVisitResult.CONTINUE;
        }
    }

    private static long jpgTime() {
        LocalDateTime localDateTime = LocalDate.now().minusDays(14).atTime(DateUtils.between(LocalTime.now().minusHours(0), LocalTime.now().plusHours(4)));
        return DateUtils.toMillis(localDateTime);
    }

    private static long xmlTime() {
        LocalDateTime localDateTime = LocalDate.now().atTime(DateUtils.between(LocalTime.now().minusHours(4), LocalTime.now().plusMinutes(5)));
        return DateUtils.toMillis(localDateTime);
    }




}
