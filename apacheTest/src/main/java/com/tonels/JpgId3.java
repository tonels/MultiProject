package com.tonels;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 测试ID生成
 * 根据指定 ID集合
 * 1、先校验源文件 PID 和 Xid 是否对应
 * 2、构建 Map 映射
 * 3、copy 文件 + ID 生成映射
 *
 */
public class JpgId3 {

    private static final Logger logger = LoggerFactory.getLogger(JpgId3.class);


    public static final String jpgPath = "\\\\192.168.1.74\\public\\testts\\all-0707\\data6\\jpg\\300-02";
    public static final String tarPath = jpgPath + "_copy";

    public static final List<Integer> picIDs = Lists.newArrayList();
    public static final List<Integer> xmlIDs = Lists.newArrayList();

    public static final HashMap<Integer, Integer> map = Maps.newHashMap();

    public static void main(String[] args) throws IOException {

        // 第一层遍历，取原ID，保证Pid与XId 一一对 应
        Path startingDir = Paths.get(jpgPath);
        Files.walkFileTree(startingDir, new FindJavaVisitor());
        Set<Integer> pIdNoxml = picIDs.stream().filter(e -> !xmlIDs.contains(e)).sorted().collect(Collectors.toCollection(LinkedHashSet::new));
        Set<Integer> XidNopic = xmlIDs.stream().filter(e -> !picIDs.contains(e)).sorted().collect(Collectors.toCollection(LinkedHashSet::new));
        logger.info("图片ID：{}个,包括{} --- xmlID：{}个,包括{} --- 无XML的图片ID:{}个,包括{} --- 无图片的XMLID:{}个,包括{}", picIDs.size(), picIDs, xmlIDs.size(), xmlIDs, pIdNoxml.size(), pIdNoxml, XidNopic.size(), XidNopic);

        // 构建 Map 映射
        // 原 ID -> 指定 序列
        // 指定 3500-3900
        List<Integer> range = IntStream.rangeClosed(3500, 3900)
                .boxed().collect(Collectors.toList());

        for (int i = 0; i < picIDs.size(); i++) {
            map.put(picIDs.get(i), range.get(i));
        }
        System.out.println(map);

        // 校验 Map 是否正确，Value 是否重复，key 是否缺漏
//        HashSet<Integer> vv = Sets.newHashSet();
//        HashSet<Integer> kk = Sets.newHashSet();
//        map.forEach((k,v) ->{
//            kk.add(k);
//            vv.add(v);
//        });
//        System.out.println("map 长度" +kk.size()+"VV长度"+vv.size());

        // 第二层遍历 copy

        Files.walkFileTree(Paths.get(jpgPath), new FindJavaVisitor2(tarPath));
    }

    private static class FindJavaVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();
            if (name.endsWith(".jpg")) {
                String substring = name.substring(0, name.length() - 4);
                picIDs.add(Integer.valueOf(substring));
            } else if (name.endsWith(".xml")) {
                String substring = name.substring(0, name.length() - 4);
                xmlIDs.add(Integer.valueOf(substring));
            }
            return FileVisitResult.CONTINUE;
        }
    }


    private static class FindJavaVisitor2 extends SimpleFileVisitor<Path> {
        private String tarPath;

        public FindJavaVisitor2(String tarPath) {
            this.tarPath = tarPath;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();
            String substring1 = name.substring(0, name.length() - 4);
            Integer integer = map.get(Integer.valueOf(substring1));
            System.out.println(substring1 + "<-->" + integer);

            if (Files.notExists(Paths.get(tarPath))) {
                Files.createDirectories(Paths.get(tarPath));
            }

            if (name.endsWith(".jpg")) {
                FileUtils.copyFile(file.toFile(), FileUtils.getFile(tarPath, integer.toString() + ".jpg"));
            } else if (name.endsWith(".xml")) {
                FileUtils.copyFile(file.toFile(), FileUtils.getFile(tarPath, integer.toString() + ".xml"));
            }
            return FileVisitResult.CONTINUE;
        }
    }

}
