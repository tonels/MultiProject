package com.tonels.fileUtil;

import com.google.common.collect.Lists;
import com.sun.webkit.network.URLs;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.junit.Test;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;

public class ApacheFileDemo {

    /**
     * 已测试
     * 先校验文件是否存在
     * 目标URI 可在可不在（已存在会替换处理）
     *
     * @throws IOException
     */
    @Test
    public void t1() throws IOException {
        String source = "E:\\test\\p1.png";
        String target = "E:\\test\\p1_1.png";

        // false 不记录修改日期，只记录创建日期
        FileUtils.copyFile(FileUtils.getFile(source), FileUtils.getFile(target), false);
    }

    /**
     * 先校验文件是否存在
     * 在校验是否是文件夹
     * 文件夹 可在可不在
     * 目标文件夹已存在时，复制原目录到目标目录，同名文件会覆盖，追加在异名文件后（存于同一目标文件夹下）
     * 目标文件夹不存在时，会新建文件夹（包括层级），在复制文件夹包括目录
     *
     * @throws IOException
     */
    @Test
    public void t2() throws IOException {
        String source = "E:\\test";
        String target = "E:\\test2.txt";

        FileUtils.copyDirectory(FileUtils.getFile(source), FileUtils.getFile(target));
    }

    /**
     * 测试 FileFilter ，嵌套文件夹不通过
     *
     * @throws IOException
     */
    @Test
    public void t3() throws IOException {
        String source = "E:\\test";
        String target = "E:\\test2";

        FileUtils.copyDirectory(FileUtils.getFile(source), FileUtils.getFile(target), filter);
    }

    FileFilter filter = new FileFilter() {
        //Override accept method
        public boolean accept(File file) {
            //if the file extension is .log return true, else false
            System.out.println(file.getName());
            if (file.getName().endsWith(".png")) {
                return true;
            }
            return false;
        }
    };


    /**
     * 先校验source是否存在
     * 在校验是否是文件
     * 文件夹 可在可不在
     *
     * @throws IOException
     */
    @Test
    public void t4() throws IOException {
        String source = "E:\\test\\test1.txt";
        String target = "E:\\test2";
        FileUtils.copyFileToDirectory(FileUtils.getFile(source), FileUtils.getFile(target));
    }

    /**
     * 将文件夹（包括源目录都复制到指定目录下）
     * 如果目录已存在,会替换整个目录
     *
     * @throws IOException
     */
    @Test
    public void t5() throws IOException {
        String source = "E:\\test";
        String target = "E:\\test2";
        FileUtils.copyDirectoryToDirectory(FileUtils.getFile(source), FileUtils.getFile(target));
    }


    /**
     * 这里 文件和目录 都适用的API
     *
     * @throws IOException
     */
    @Test
    public void t6() throws IOException {
        String source = "E:\\test\\test1.txt";
        String target = "E:\\test2";
        FileUtils.copyToDirectory(FileUtils.getFile(source), FileUtils.getFile(target));
    }

    /**
     * 下载
     *
     * @throws IOException
     */
    @Test
    public void t7() throws IOException {
        String source = "https://mirrors.aliyun.com/apache/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz";
        String target = "E:\\apache-flume-1.9.0-bin.tar.gz";

        URL url = URLs.newURL(source);

        FileUtils.copyURLToFile(url, FileUtils.getFile(target));
    }

    /**
     * 创建文件夹，可递归
     * 很常用
     */
    @Test
    public void t8() throws IOException {
        String source = "D:\\apache\\sds\\sd\\aa.txt";

        FileUtils.forceMkdir(FileUtils.getFile(source));
//        FileUtils.forceMkdirParent(FileUtils.getFile(source));
    }

    @Test
    public void t9() throws IOException {
        String source = "D:\\apache\\sds\\sd\\aa.txt";
        File file = new File(source); // 不存在
        if (file.exists()) {
            System.out.println("存在");
        } else {
            System.out.println("不存在");
        }

        File file1 = FileUtils.getFile(source);
        if (file1.exists()) {
            System.out.println("存在");
        } else {
            System.out.println("不存在");
        }
    }

    /**
     * 清空文件夹
     *
     * @throws IOException
     */
    @Test
    public void t10() throws IOException {
        String source = "D:\\apache\\sds\\sd\\";
        FileUtils.cleanDirectory(FileUtils.getFile(source));
    }

    /**
     * 获取临时文件夹
     * 中间数据流 会用到
     *
     * @throws IOException
     */
    @Test
    public void t11() throws IOException {
        File tempDirectory = FileUtils.getTempDirectory();
        System.out.println(tempDirectory.getAbsolutePath());
    }

    /**
     * log
     * SpringAll-master
     * SpringAll-master.zip
     *
     * @throws IOException
     */
    @Test
    public void t13() throws IOException {
        final File file = new File("D:\\test");
        final String[] list = file.list();

        for (String s : list) {
            System.out.println(s);
        }
    }

    /**
     * 后缀名 - 过滤
     *
     * @throws IOException
     */
    @Test
    public void t15() throws IOException {
        File dir = new File("D:\\test\\");
//        List<File> fileList = (List<File>)FileUtils.listFiles(dir,new String[]{"txt"},true);//列出该目录下的所有txt文件，递归（扩展名不必带.doc）
//        List<File> fileList = (List<File>) FileUtils.listFiles(dir, new String[]{"txt"}, false);//列出该目录下的所有文件，递归
//        List<File> fileList = (List<File>)FileUtils.listFiles(dir,null,false);//列出该目录下的所有文件，不递归

        final List<File> fileList = Lists.newArrayList(FileUtils.listFiles(dir, new String[]{"txt"}, false));
        fileList.stream().forEach(file -> System.out.println(file.getAbsolutePath()));
    }

    /**
     * todo 待测
     *
     * @throws IOException
     */
    @Test
    public void t16() throws IOException {
        File dir = new File("D:\\test\\");
        final Collection<File> files = FileUtils.listFiles(dir, new IOFileFilter() {
            @Override
            public boolean accept(File file) {
                return false;
            }

            @Override
            public boolean accept(File dir, String name) {
                return false;
            }
        }, new IOFileFilter() {
            @Override
            public boolean accept(File file) {
                return false;
            }

            @Override
            public boolean accept(File dir, String name) {
                return false;
            }
        });
        files.stream().forEach(file -> System.out.println(file.getAbsolutePath()));
    }

    @Test
    public void t18() throws IOException {
        FileUtils.write(new File("D:\\test\\da\\dsad\\ttt.txt"), "sdsa", Charset.defaultCharset());
    }


}
