package com.tonels;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件筛选（ xml 和对应的 jpg ）
 */
public class FileTest {
    List<String> name = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        String source = "E:\\aiPic\\ls\\xj";
        String target = "E:\\aiPic\\ls\\opeLater";
        List<String> name = new ArrayList<>();

        // 筛选复制 XML
        FileUtils.copyDirectory(FileUtils.getFile(source), FileUtils.getFile(target), new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                //if the file extension is .log return true, else false
                if (pathname.getName().endsWith(".xml")) {
                    System.out.println(StringUtils.substring(pathname.getName(), 0, pathname.getName().length() - 4));
                    name.add(StringUtils.substring(pathname.getName(), 0, pathname.getName().length() - 4));
                    return true;
                }
                return false;
            }
        });

        // 根据 XML 筛选 jpg
        FileUtils.copyDirectory(FileUtils.getFile(source), FileUtils.getFile(target), new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.getName().endsWith(".jpg")) {
                    String substring = StringUtils.substring(pathname.getName(), 0, pathname.getName().length() - 4);
                    if (name.contains(substring)) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

}
