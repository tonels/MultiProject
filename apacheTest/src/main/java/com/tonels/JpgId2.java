package com.tonels;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * ID 重新生成
 */
public class JpgId2 {

    private static final Logger logger = LoggerFactory.getLogger(JpgId2.class);

    public static final List<Integer> picID1s = Lists.newArrayList();
    public static final List<Integer> picID2s = Lists.newArrayList();


    public static final Set<Integer> xmlIDs = Sets.newTreeSet((o1, o2) -> o1.compareTo(o2));

    public static final Map<Integer, Integer> map = new LinkedHashMap<Integer, Integer>();

    public static void main(String[] args) throws IOException {

        // 第一层遍历,
        String IDpath1 = "\\\\192.168.1.74\\public\\testts\\all-0707\\data7\\test\\632";
        Files.walkFileTree(Paths.get(IDpath1), new FindJavaVisitor1());
        logger.info("jpg->{},xml->{}", picID1s.size(), xmlIDs.size());

        // 第二层遍历，合并 Map
        String originPath = "\\\\192.168.1.74\\public\\testts\\all-0707\\data7\\zhoudanhua";
        Files.walkFileTree(Paths.get(originPath), new FindJavaVisitor2());

        // 构建 Map
        for (int i = 0; i < picID2s.size(); i++) {
            map.put(picID2s.get(i), picID1s.get(i));
        }
        System.out.println(map.toString());


        // 校验 Map 是否正确，Value 是否重复，key 是否缺漏
//        HashSet<Integer> objects = Sets.newHashSet();
//        map.forEach((k,v) ->{
//            objects.add(v);
//        });
//        System.out.println(objects.size());

        // 第三层遍历，copy 文件
        String originPath3 = "\\\\192.168.1.74\\public\\testts\\all-0707\\data7\\zhoudanhua";
        String tarPath = "\\\\192.168.1.74\\public\\testts\\all-0707\\data7\\huizong\\632";

//        String oriPath = "\\\\192.168.1.74\\public\\testts\\ll";
//        String tarPath = "\\\\192.168.1.74\\public\\testts\\lls";
        Files.walkFileTree(Paths.get(originPath3), new FindJavaVisitor3(tarPath));
    }

    private static class FindJavaVisitor1 extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();

            if (name.endsWith(".jpg")) {
                String substring = name.substring(0, name.length() - 4);
                picID1s.add(Integer.valueOf(substring));
            }
            return FileVisitResult.CONTINUE;
        }
    }

    private static class FindJavaVisitor2 extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();

            if (name.endsWith(".jpg")) {
                String substring = name.substring(0, name.length() - 4);
                picID2s.add(Integer.valueOf(substring));
            }
            return FileVisitResult.CONTINUE;
        }
    }


    private static class FindJavaVisitor3 extends SimpleFileVisitor<Path> {
        public FindJavaVisitor3(String tarPath) {
            this.tarPath = tarPath;
        }

        private String tarPath;

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();

            String substring1 = name.substring(0, name.length() - 4);

            Integer integer = map.get(Integer.valueOf(substring1));
            System.out.println(substring1 + integer);

            if (name.endsWith(".jpg")) {
                FileUtils.copyFile(file.toFile(), FileUtils.getFile(tarPath, integer.toString() + ".jpg"));
            } else if (name.endsWith(".xml")) {
                FileUtils.copyFile(file.toFile(), FileUtils.getFile(tarPath, integer.toString() + ".xml"));
            }
            return FileVisitResult.CONTINUE;
        }
    }


}
