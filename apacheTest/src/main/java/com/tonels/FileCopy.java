package com.tonels;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FileCopy {

    private static final Logger logger = LoggerFactory.getLogger(FileCopy.class);

    public static final List<Integer> picID1s = Lists.newArrayList();
    public static final List<Integer> picID2s = Lists.newArrayList();



    public static final Set<Integer> xmlIDs = Sets.newTreeSet((o1, o2) -> o1.compareTo(o2));

//    public static final Map<String, String> map = Maps.newHashMap();
    public static final Map<Integer, Integer> map = new LinkedHashMap<Integer, Integer>();

    public static void main(String[] args) throws IOException {

        // 第一层遍历,
        String IDpath = "\\\\192.168.1.74\\public\\testts\\2+3";
        String tarpath = "\\\\192.168.1.74\\public\\testts\\ds";
        Files.walkFileTree(Paths.get(IDpath), new FindJavaVisitor1(tarpath));
        logger.info("jpg->{},xml->{}", picID1s.size(), xmlIDs.size());
    }

    private static class FindJavaVisitor1 extends SimpleFileVisitor<Path> {
        private  String tarpath;
        public FindJavaVisitor1(String tarpath) {
            this.tarpath = tarpath;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            File file1 = file.toFile();
            String name = file1.getName();

            if (name.endsWith(".jpg")) {
                String substring = name.substring(0, name.length() - 4);
                picID1s.add(Integer.valueOf(substring));

                FileUtils.copyFile(file.toFile(),FileUtils.getFile(tarpath,"ss" + ".jpg"));
            }
            return FileVisitResult.CONTINUE;
        }
    }


}
