import com.tonels.util.DateUtils;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Timetest {

    @Test
    public void t1() {
//        System.out.println(System.currentTimeMillis());
//        System.out.println(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli());
//
////        LocalDate.
//
//        System.out.println(LocalTime.now());
//
//        LocalTime between = DateUtils.between(LocalTime.now().minusHours(6), LocalTime.now().plusHours(5));
//
//        LocalDateTime localDateTime = between.atDate(LocalDate.now());
//        System.out.println(between.toString());
//        System.out.println(localDateTime.toString());


        LocalDateTime localDateTime = LocalDate.now().minusDays(5).atTime(DateUtils.between(LocalTime.now().minusHours(6), LocalTime.now().plusHours(5)));
        System.out.println(localDateTime.toString());

    }


    @Test
    public void t2() throws ParseException {

        LocalDateTime startTime = DateUtils.parseLocalDateTime("2020-06-23 13:00:00");
        LocalDateTime endTime = DateUtils.parseLocalDateTime("2020-06-23 16:00:00");
        System.out.println(startTime);

    }




}
