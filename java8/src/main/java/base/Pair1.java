package base;

import com.google.common.base.Objects;

public class Pair1 {
    private String name;
    private Double version;

    public Pair1(String name, Double version) {
        this.name = name;
        this.version = version;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair1 pair1 = (Pair1) o;
        return Objects.equal(name, pair1.name) && Objects.equal(version, pair1.version);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, version);
    }


    @Override
    public String toString() {
        return "Pair1{" +
                "name='" + name + '\'' +
                ", version=" + version +
                '}';
    }
}
