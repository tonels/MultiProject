package base;

public class Tss {

    public static void main(String[] args) {

        String a1 = new String("AA") + new String("BB");
        System.out.println("a1 == a1.intern() " + (a1 == a1.intern())); // true

        String test = "ABABCDCD";
        String a2 = new String("ABAB") + new String("CDCD");
        String a3 = "ABAB" + "CDCD";

        System.out.println("a3==test " + (a3 == test)); // true
        System.out.println("a2==test " + (a2 == test)); // false

        System.out.println("a2 == a2.intern() " + (a2 == a2.intern())); // false
        System.out.println("a2 == a3 " + (a2 == a3)); // false
        System.out.println("a3 == a2.intern() " + (a3 == a2.intern())); // true
        System.out.println("a2==a3.intern()  " + (a2 == a3.intern())); // false


        System.out.println("a3==test " + (a3 == test)); // true
        System.out.println("a2==test " + (a2 == test)); // false

        System.out.println("test==a2.intern()  " + (test == a2.intern())); // true
        System.out.println("test==a3.intern()  " + (test == a3.intern())); // true


    }




}
