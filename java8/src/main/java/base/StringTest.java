package base;

public class StringTest {

    public static void main(String[] args) {
        t1();

        t2();

    }

    public static void t1(){
        System.out.println("START");
        b: {
            for (int i = 0; i < 10; i++ ) {
                System.out.println("START-" + i + "---------------");
                for (int j = 0; j < 10; j++) {
                    System.out.println(j);
                    if (j == 3) {
                        break b;
                    }
                }
                System.out.println("END-" + i + "---------------");
            }
        }
        System.out.println("END");
    }

    public static void t2() {
        System.out.println("START");
            for (int i = 0; i < 10; i++) {
                System.out.println("START-" + i + "---------------");
                for (int j = 0; j < 10; j++) {
                    System.out.println(j);
                    if (j == 3) {
                        break ;
                    }
                }
                System.out.println("END-" + i + "---------------");
            }
        System.out.println("END");
    }









}
