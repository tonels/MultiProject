package base;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueTest {
    public static void main(String[] args) {

        int[] a = {2,9,3};
        int[] b = {2,9,3};

        negativePrint(a);
        positivePrint(b);
    }


    private static void negativePrint(int[] nums) {
        PriorityQueue<Integer> queue=new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2-o1;
            }
        });
        for(int temp:nums){
            queue.add(temp);
        }
        System.out.println();
        System.out.print("倒序输出：");
        for(int i=0;i<3;i++){
            System.out.print(queue.poll()+" ");
        }
    }

    private static void positivePrint(int[] nums){
        PriorityQueue<Integer> queue=new PriorityQueue<>();
        for(int temp:nums){
            queue.add(temp);
        }
        System.out.print("正序输出：");
        for(int i=0;i<3;i++){
            System.out.print(queue.poll()+" ");
        }

    }












}
