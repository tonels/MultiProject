package base;

import java.util.HashMap;

/**
 * @program: MultiProject
 * @description: 测试hashMaP
 * @author: liangshuai
 * @create: 2020-11-06 10:28
 **/
public class HashMp {

    public static void main(String[] args) {

//        t1();
        t2();

    }

    public static void t1() {

//        HashMap<String, Object> map = Maps.newHashMap();

        HashMap<String, Object> oHashmap = new HashMap<>();
        oHashmap.put("a", 1);
        oHashmap.put("b", 2);
        HashMap<String, Object> oHashmap2 = new HashMap<>();
        oHashmap2.put("a", 1);

    }

    public static void t2() {
        int i = 2 ^ 30;
        int i2 = 2 ^ 31;
        int i3 = 2 ^ 32;
        int i4 = 2 ^ 3;

        System.out.println("i = " + i);
        System.out.println("i2 = " + i2);
        System.out.println("i3 = " + i3);
        System.out.println("i4 = " + i4);


        int ii1 = 1 << 30;
        int ii2 = 1 << 31;
        int ii3 = 1 << 32;
        System.out.println("ii1 = " + ii1);
        System.out.println("ii2 = " + ii2);
        System.out.println("ii3 = " + ii3);

        int maxValue = Integer.MAX_VALUE;
        System.out.println("maxValue = " + maxValue);


    }

    public static void t3() {
//        List<Pair1<String, Double>> pairArrayList = new ArrayList(3);
//        pairArrayList.add(new Pair1<>("version", 12.10));
//        pairArrayList.add(new Pair1<>("version", 12.19));
//        pairArrayList.add(new Pair1<>("version", 6.28));
//

    }


}
