package base;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import samples.lambda.Person2;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 */
public class BreakTest {

    private List<Person2> list;
    private List<String> strings;

    // 初始化数据需要的
    @Before
    public void inti() {
        Person2 p1 = new Person2(13, "男", "张2");
        Person2 p2 = new Person2(2, "男", "张6");
        Person2 p3 = new Person2(4, "男", "张1");
        Person2 p4 = new Person2(6, "女", "张3");
        Person2 p5 = new Person2(1, "男", "张5");
        Person2 p6 = new Person2(10, "男", "张8");
        Person2 p7 = new Person2(3, "女", "张4");
        Person2 p8 = new Person2(31, "女", "张4");
        Person2 p9 = new Person2(21, "女", "张4");
        Person2 p10 = new Person2(19, "女", "张4");

        list = Lists.newArrayList(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
        strings = Lists.newArrayList("a1", "a2", "b1", "c2", "c1");
    }

    /**
     * p.getId() = 13
     * 集合中碰到满足条件的(p.getId()>=13) -> 13
     * 循环已结束
     */
    @Test
    public void t1() {

        for (Person2 p : list) {
            System.out.println("p.getId() = " + p.getId());
            if (p.getId()>=13) {
                System.out.println("集合中碰到满足条件的(p.getId()>=13) -> " + p.getId());
                break;
            }
            System.out.println(p.toString());
        }
        System.out.println("循环已结束");
    }

    /**
     * 继续循环体
     * Person2(id=2, sex=男, lastName=张6)
     * Person2(id=4, sex=男, lastName=张1)
     * Person2(id=6, sex=女, lastName=张3)
     * Person2(id=1, sex=男, lastName=张5)
     * Person2(id=10, sex=男, lastName=张8)
     * Person2(id=3, sex=女, lastName=张4)
     */
    @Test
    public void t2() {
        for (Person2 p : list) {
            System.out.println("p.getId() = " + p.getId());
            if (p.getId()>=13) {
                System.out.println("集合中碰到满足条件的(p.getId()>=13) -> " + p.getId());
                continue;
            }
            System.out.println(p.toString());
        }
        System.out.println("循环已结束");
    }


}
