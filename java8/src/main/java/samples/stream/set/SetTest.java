package samples.stream.set;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.Sets;
import com.google.common.collect.TreeRangeSet;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SetTest {

    /**
     * Set 三种方式遍历
     */
    @Test
    public void t1() {
        Set<Integer> sets = Sets.newHashSet(234567489, 2132, 11, 5, 4, 2, 1, 1, 2, 3);

        // 第一种
        sets.forEach(System.out::println);

        // 第二种
        Iterator<Integer> it = sets.iterator();
        while (it.hasNext()) {
            Integer str = it.next();
            System.out.println(str);
        }

        // 第三种
        for (Integer str : sets) {
            System.out.println(str);
        }
    }

    /**
     * 生成区间列表
     */
    @Test
    public void t2() {
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        rangeSet.add(Range.closed(3, 10));
    }

    @Test
    public void t3() {
        List<Integer> range = IntStream.rangeClosed(0, 10)
                .boxed().collect(Collectors.toList());
        for (int i = 0; i < range.size(); i++) {
            System.out.println(range.get(i));
        }


    }


}
