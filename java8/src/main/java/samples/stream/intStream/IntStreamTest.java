package samples.stream.intStream;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntStreamTest {

    /**
     * col_0 sa,
     * col_1 aw,
     * col_2 ar,
     * col_3 af,
     * col_4 avb
     */
    @Test
    public void t1() {
        final List<String> strings = Lists.newArrayList("sa", "aw", "ar", "af", "avb");
        Stream<String> columnDefinitions = IntStream.range(0, strings.size())
                .mapToObj(i -> String.format("col_%d %s", i, strings.get(i)));
        final String join = Joiner.on(",\n").join(columnDefinitions.iterator());
        System.out.println(join);
    }

    /**
     * 合并流
     * b_0
     * a_1
     * a_0
     * b_2
     * a_3
     * b_1
     * a_2
     */
    @Test
    public void t2() {
        Random random = new Random();
        Set<String> keysIncludingANull = Stream.of(
                IntStream.range(0, 3).mapToObj(i -> "b_" + i),
//                Stream.of((String) null),
                IntStream.range(0, 4).mapToObj(i -> "a_" + i))
                .flatMap(s -> s)
                .collect(Collectors.toSet());
        keysIncludingANull.forEach(System.out::println);
    }

    /**
     * 合并流
     * b_0
     * a_1
     * a_0
     * b_2
     * a_3
     * b_1
     * a_2
     */
    @Test
    public void t3() {
        Set<String> keysIncludingANull = Stream.concat(
                IntStream.range(0, 3).mapToObj(i -> "b_" + i),
                IntStream.range(0, 4).mapToObj(i -> "a_" + i))
                .collect(Collectors.toSet());
        keysIncludingANull.forEach(System.out::println);
    }


}
