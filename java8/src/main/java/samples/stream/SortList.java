package samples.stream;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortList {

    @Test
    public void t1() {
        List<Integer> list = Lists.newArrayList(1, 23, 14, 2, 4, 6, 24, 5);
        System.out.println(list.stream().sorted().collect(Collectors.toList()));
        System.out.println(list.stream().sorted(new com1()).collect(Collectors.toList()));
    }

    static class com1 implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            return 0;
        }
    }


}
