package samples.stream;

import com.google.common.collect.Lists;
import samples.lambda.Person2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * todo
 * shuffle 待测试
 */
public class Generator {

    public static void main(String[] args) {
        t1();
//        t2();
//        t3();
    }

    public static void t1() {
        Person2 p1 = new Person2(13, "男", "张2");
        Person2 p2 = new Person2(2, "男", "张6");
        Person2 p3 = new Person2(4, "男", "张1");
        Person2 p4 = new Person2(6, "女", "张3");
        Person2 p5 = new Person2(1, "男", "张5");
        Person2 p6 = new Person2(10, "男", "张8");
        Person2 p7 = new Person2(3, "女", "张4");
        Person2 p8 = new Person2(31, "女", "张4");
        Person2 p9 = new Person2(21, "女", "张4");
        Person2 p10 = new Person2(19, "女", "张4");

        final ArrayList<Person2> person2s = Lists.newArrayList(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
        cyclingShuffledStream(person2s).forEach(System.out::println);
    }

    private static <T> Stream<T> cyclingShuffledStream(Collection<T> collection) {
        List<T> list = new ArrayList<>(collection);
        Collections.shuffle(list);
        return Stream.generate(() -> list).flatMap(List::stream);
    }


    public static void t2() {
        List<Integer> mutableList = new ArrayList<Integer>();
        mutableList.add(1);
        mutableList.add(2);
        mutableList.add(3);
        mutableList.add(4);
        mutableList.add(5);

        System.out.println(mutableList);

        Collections.shuffle(mutableList);
        System.out.println(mutableList);
    }

    public static void t3() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i < 11; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i = 0; i < 3; i++) {
            System.out.println(list.get(i));
        }
    }
}


