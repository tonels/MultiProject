package samples.util;

import cn.hutool.core.util.RandomUtil;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import samples.domain.DataLineage;

import java.util.List;

public class ListTest {

    private List<DataLineage> list;

    @Before
    public void inti() {
        DataLineage p1 = new DataLineage(1L,"ton","12","NAN");
        DataLineage p2 = new DataLineage(2L,"ton2","12","NAN");
        DataLineage p3 = new DataLineage(3L,"ton2","13","NAN");
        DataLineage p4 = new DataLineage(4L,"ton2","12","NAN");
        list = Lists.newArrayList(p1,p2,p3,p4);

    }

    /**
     * 根据名字除重
     * DataLineage(id=1, name=ton, age=12, sex=NAN)
     * DataLineage(id=2, name=ton2, age=12, sex=NAN)
     */
    @Test
    public void t1() {
        List<DataLineage> distinctDataLineageList = ListUtils.distinctList(list,
                DataLineage::getName);
        distinctDataLineageList.forEach(System.out::println);
    }

    /**
     * DataLineage(id=1, name=ton, age=12, sex=NAN)
     * DataLineage(id=2, name=ton2, age=12, sex=NAN)
     * DataLineage(id=3, name=ton2, age=13, sex=NAN)
     */
    @Test
    public void t2() {
        List<DataLineage> distinctDataLineageList = ListUtils.distinctList(list,
                DataLineage::getName,
                DataLineage::getAge
                );
        distinctDataLineageList.forEach(System.out::println);
    }


}
