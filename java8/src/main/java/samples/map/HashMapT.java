package samples.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Test;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

public class HashMapT {

    /**
     * HashMap 测试，
     * {a=3}
     * {a=3}
     * {200=3}
     * {MapT.Demo(id=1)=3}
     */
    @Test
    public void t1() {
        Map<String, String> map = new HashMap<>();
        map.put("a", "1");
        map.put("a", "2");
        map.put("a", "3");
        System.out.println(map);

        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("a", "1");
        hashMap.put("a", "2");
        hashMap.put("a", "3");
        System.out.println(hashMap);

        Map<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(200, "1");
        hashMap2.put(new Integer(200), "2");
        hashMap2.put(new Integer(200), "3");
        System.out.println(hashMap2);

        Map<Demo, String> hashMap3 = new HashMap<>();
        hashMap3.put(new Demo(1), "1");
        hashMap3.put(new Demo(1), "2");
        hashMap3.put(new Demo(1), "3");
        System.out.println(hashMap3);
    }

    @Data
    @AllArgsConstructor
    private class Demo {
        private Integer id;
    }


    /**
     * IdentityHashMap 测试
     * {a=2, a=1, a=3}
     * {MapT.Demo(id=1)=2, MapT.Demo(id=1)=1, MapT.Demo(id=1)=3}
     */
    @Test
    public void t2() {
        Map<String, String> identityHashMap = new IdentityHashMap<>();
        identityHashMap.put(new String("a"), "1");
        identityHashMap.put(new String("a"), "2");
        identityHashMap.put(new String("a"), "3");
        System.out.println(identityHashMap);

        System.out.println(identityHashMap.get("a"));

        Map<Demo, String> identityHashMap2 = new IdentityHashMap<>();
        identityHashMap2.put(new Demo(1), "1");
        identityHashMap2.put(new Demo(1), "2");
        identityHashMap2.put(new Demo(1), "3");
        System.out.println(identityHashMap2);

    }

    /**
     * 计算词频
     */
    @Test
    public void t3() {
        String str = "From configuration to security, web apps to big data—whatever the infrastructure needs of your application may be, there is a Spring Project to help you build it. Start small and use just what you need—Spring is modular by design.";

        // jdk1.8之前的写法
        HashMap<Character, Integer> result1 = new HashMap<>(32);
        for (int i = 0; i < str.length(); i++) {
            char curChar = str.charAt(i);
            Integer curVal = result1.get(curChar);
            if (curVal == null) {
                curVal = 1;
            } else {
                curVal += 1;
            }
            result1.put(curChar, curVal);
        }
        result1.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });

    }

    /**
     * compute 示例
     */
    @Test
    public void t4() {
        String str = "From configuration to security, web apps to big data—whatever the infrastructure needs of " +
                "your application may be, there is a Spring Project to help you build it. " +
                "Start small and use just what you need—Spring is modular by design.";

        // jdk1.8的写法
        HashMap<Character, Integer> result2 = new HashMap<>(32);
        for (int i = 0; i < str.length(); i++) {
            char curChar = str.charAt(i);
            result2.compute(curChar, (k, v) -> {
                if (v == null) {
                    v = 1;
                } else {
                    v += 1;
                }
                return v;
            });
        }
        result2.forEach((k, v) -> {
            System.out.println(k + ":" + v);
        });
    }
}
