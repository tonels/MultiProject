package samples.function.functionalinterface;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class _Ls {
    public static void main(String[] args) {
        // 练习 function
//        System.out.println(f1_add.apply(2));
//        System.out.println(aTest.apply(2));
//        System.out.println(a2.apply(0));
        // 练习 Consumer
//        c1.accept(2);
        // 练习predicate
//        System.out.println(p1.test("io"));

        // 测试 supplier
        System.out.println(s1.get());
    }


    static Function<Integer, Integer> f1_add = new Function<Integer, Integer>() {
        @Override
        public Integer apply(Integer integer) {
            return integer + 1;
        }
    };


    static Function<Integer, A> aTest = new Function<Integer, A>() {
        @Override
        public A apply(Integer integer) {
            return new A(integer, integer + "");
        }
    };

    static Function<Integer, A> a2 =
            a -> {
                if (a.equals(0)) {
                    try {
                        throw
                                new Exception("参数错误");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return new A(a,null);
            };

    static Consumer<Integer> c1 = integer -> {
        int i = integer + 1;
        System.out.println(i);
    };


    static Predicate<String> p1 = new Predicate<String>() {
        @Override
        public boolean test(String s) {
            return false;
        }
    };

    static Supplier<String> s1 = new Supplier<String>() {
        @Override
        public String get() {
            return "sss";
        }
    };











    static class A {
        private Integer a1;
        private String a2;

        public A(Integer a1, String a2) {
            this.a1 = a1;
            this.a2 = a2;
        }

        public Integer getA1() {
            return a1;
        }

        public void setA1(Integer a1) {
            this.a1 = a1;
        }

        public String getA2() {
            return a2;
        }

        public void setA2(String a2) {
            this.a2 = a2;
        }

        @Override
        public String toString() {
            return "A{" +
                    "a1=" + a1 +
                    ", a2='" + a2 + '\'' +
                    '}';
        }
    }

}
