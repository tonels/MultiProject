package samples.concurrent;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class AtomicTest {

    /**
     * Erik
     * Erik
     * Erik
     * Erik
     * Erik
     * Erik
     * Erik
     * Erik
     */
    @Test
    public void t1(){
        String[] names = {"sa", "Pamela", "Dave", "Pascal", "Erik","Erik","Erik","Erik"};
        AtomicInteger index = new AtomicInteger();
        List<String> list = Arrays.stream(names)
                .filter(n -> n.length() <= index.incrementAndGet())
                .peek(System.out::println)
                .collect(Collectors.toList());

        list.forEach(System.out::println);
    }



}
