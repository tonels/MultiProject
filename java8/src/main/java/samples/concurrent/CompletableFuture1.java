package samples.concurrent;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public class CompletableFuture1 {

    /**
     * 42
     * done
     *
     * @param args
     */
    public static void main(String[] args) {
//        t1();
        t2(null);
    }

    private static void t1() {
        CompletableFuture<String> future = new CompletableFuture<>();

        future.complete("42");

        future
                .thenAccept(System.out::println)
                .thenAccept(v -> System.out.println("done"));
    }

    private static void t2(ExecutorService executorService) {
        CompletableFuture<Void> cf;

        if (null == executorService) {
            cf = CompletableFuture.runAsync(() -> {
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName());
            });
        } else {
            cf = CompletableFuture.runAsync(() -> {
                try {
                    Thread.sleep(3000L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName());
            }, executorService);
        }
        System.out.println(Thread.currentThread().getName() + ":" + cf.isDone());

        while (true) {
            if (cf.isDone()) {
                System.out.println(Thread.currentThread().getName() + " is done");
                break;
            }
        }

    }


}
