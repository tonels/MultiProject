package samples.concurrent;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * 这里测试AtomicInteger，原子操作
 * 源码中 AtomicInteger 中
 * 方法多是 Final 修饰的，
 * 还有变量 private volatile int value
 */
@Slf4j
public class Atomic1 {

    private static final int NUM_INCREMENTS = 100000;

    private static AtomicInteger atomicInt = new AtomicInteger(0);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        testIncrement();
//        testAccumulate();
//        testUpdate();
    }

    // 更新
    private static void testUpdate() {
        atomicInt.set(0);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, NUM_INCREMENTS)
                .forEach(i -> {
                    Runnable task = () ->
                            atomicInt.updateAndGet(n -> n + 2);
                    executor.submit(task);
                });

        ConcurrentUtils.stop(executor);

        System.out.format("Update: %d\n", atomicInt.get());
    }

    // 计算
    private static void testAccumulate() {
        atomicInt.set(0);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, NUM_INCREMENTS)
                .forEach(i -> {
                    Runnable task = new Runnable() {
                        @Override
                        public void run() {
                            atomicInt.accumulateAndGet(i, (n, m) -> n + m);
                        }
                    };
                    executor.submit(task);
                });

        ConcurrentUtils.stop(executor);

        System.out.format("Accumulate: %d\n", atomicInt.get());
    }

    // 增加
    private static void testIncrement() throws ExecutionException, InterruptedException {
        atomicInt.set(0);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        for (int i = 0; i < NUM_INCREMENTS; i++) {
//            final Future<Integer> submit = executor.submit(() -> atomicInt.incrementAndGet());
            executor.submit(new IncreTask(atomicInt));
            log.info("{},当前值为{}",Thread.currentThread().getName(),atomicInt);
        }

        ConcurrentUtils.stop(executor);

        System.out.format("Increment: Expected=%d; Is=%d\n", NUM_INCREMENTS, atomicInt.get());
    }


    static class IncreTask implements Runnable{
        AtomicInteger atomicInt = new AtomicInteger(0);

        public IncreTask(AtomicInteger atomicInt) {
            this.atomicInt = atomicInt;
        }

        @Override
        public void run() {
            atomicInt.incrementAndGet();
            log.info("当前线程：{}",Thread.currentThread().getName());

        }
}


}
