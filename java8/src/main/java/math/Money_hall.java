package math;

import java.util.Random;
import java.util.Scanner;

public class Money_hall {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入测试次数：");
        //输入测试次数
        double testTimes = sc.nextInt();
        System.out.println("开始计算\n");
        Random r = new Random();
        //换门输赢测试
        //初始化赢与输的次数
        int Win_ChangeDoor = 0, Lost_InitialDoor = 0;
        //for循环，实现多次的数据测试
        for (int i = 0; i < testTimes; i++) {
            //随机生成赢的门号与猜测的门号(1-3)
            int winNumber = r.nextInt(3) + 1;
            int selectedNumbers = r.nextInt(3) + 1;
            //当赢的值与猜测的值不一样时，换门必定赢，因为换门时排除了一个错误答案,自己猜的又是错的
            if (winNumber != selectedNumbers) {
                //增加赢的次数
                Win_ChangeDoor++;
            } else Lost_InitialDoor++;//增加输的次数
        }
        System.out.println("换门赢概率\t " + Double.parseDouble(String.format("%.2f", (Win_ChangeDoor / testTimes) * 100)) + "%\t 赢了" + Win_ChangeDoor + "次   输了" + Lost_InitialDoor + "次\n");


        //不换门输赢测试
        //初始化赢与输的次数
        Win_ChangeDoor = 0;
        Lost_InitialDoor = 0;
        //for循环，实现多次的数据测试
        for (int i2 = 0; i2 < testTimes; i2++) {
            //随机生成赢的门号与猜测的门号(1-3)
            int winNumber = r.nextInt(3) + 1;
            int selectedNumbers = r.nextInt(3) + 1;
            //不换门时很简单，猜对就是赢，猜错就是输
            if (winNumber == selectedNumbers) {
                //增加赢的次数
                Win_ChangeDoor++;
            } else Lost_InitialDoor++;//增加输的次数
        }
        //输出不换门与换门的最终结果
        System.out.println("不换门赢概率\t " + Double.parseDouble(String.format("%.2f", (Win_ChangeDoor / testTimes) * 100)) + "%\t 赢了" + Win_ChangeDoor + "次   输了" + Lost_InitialDoor + "次");
    }
}
