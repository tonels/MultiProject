package tonels.hutool.tree;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.TreeNode;
import lombok.Data;

import java.util.List;

public class TrVOTest extends TreeNode {

    static List<TrVO> nodeList = CollUtil.newArrayList();

    static {
        // 模拟数据
        nodeList.add(new TrVO("1", "0", "系统管理", 5,1));
        nodeList.add(new TrVO("11", "1", "用户管理", 222222,2));
        nodeList.add(new TrVO("111", "11", "用户添加", 0,3));

        nodeList.add(new TrVO("2", "0", "店铺管理", 1,1));
        nodeList.add(new TrVO("21", "2", "商品管理", 44,2));
        nodeList.add(new TrVO("221", "2", "商品管理2", 2,2));
        nodeList.add(new TrVO("2221", "221", "商品管理2", 2,2));
        nodeList.add(new TrVO("22221", "2221", "商品管理2", 2,2));
        nodeList.add(new TrVO("222221", "22221", "商品管理2", 2,2));
    }



    public static void main(String[] args) {

    }

}




@Data
class TrVO extends TreeNode{
    public Integer level;

    public TrVO(Object id, Object parentId, String name, Comparable weight, Integer level) {
        super(id, parentId, name, weight);
        this.level = level;
    }
}