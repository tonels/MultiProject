package cn.hutool.core.lang.tree;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试可从 hutool tree工具类中构建树、向上找父，向下子树
 */
public class TreeSearchTest {
    static List<TreeNode<Long>> all_menu = new ArrayList<>();

    static {
        /*
         * root
         *    /module-A
         *    	   /module-A-menu-1
         *    /module-B
         *    	   /module-B-menu-1
         *    	   /module-B-menu-2
         *    	        /module-B-menu-2-1
         *    	        /module-B-menu-2-2
         */
        all_menu.add(new TreeNode<>(1L, 0L, "root", 0L));

        all_menu.add(new TreeNode<>(2L, 1L, "module-A", 0L));
        all_menu.add(new TreeNode<>(3L, 1L, "module-B", 0L));

        all_menu.add(new TreeNode<>(4L, 2L, "module-A-menu-1", 0L));
        all_menu.add(new TreeNode<>(5L, 3L, "module-B-menu-1", 0L));
        all_menu.add(new TreeNode<>(6L, 3L, "module-B-menu-2", 0L));

        all_menu.add(new TreeNode<>(7L, 6L, "module-B-menu-2-1", 0L));
        all_menu.add(new TreeNode<>(8L, 6L, "module-B-menu-2-2", 1L));
    }

    /**
     * 根据父节点查找子节点树
     */
    @Test
    public void searchNode() {
        List<Tree<Long>> treeItems = TreeUtil.build(all_menu, 0L);

        Tree<Long> tree = treeItems.get(0);
        Tree<Long> searchResult = tree.getNode(3L);

        Tree<Long> longTree2 = searchResult.getNode(2L);
        Tree<Long> longTree3 = searchResult.getNode(3L);


        System.out.println(JSON.toJSONString(searchResult, SerializerFeature.PrettyFormat));
        System.out.println(JSON.toJSONString(longTree2, SerializerFeature.PrettyFormat));
        System.out.println(JSON.toJSONString(longTree3, SerializerFeature.PrettyFormat));

    }

    @Test
    public void searchPNode() {
        List<Tree<Long>> treeItems = TreeUtil.build(all_menu, 0L);

        Tree<Long> tree = treeItems.get(0);
        Tree<Long> tree2 = tree.getNode(6L);

        List<CharSequence> list = tree2.getParentsName(false);
        System.out.println(JSON.toJSONString(list, SerializerFeature.PrettyFormat));

        Tree<Long> parent = tree2.getParent();
        System.out.println(JSON.toJSONString(parent, SerializerFeature.PrettyFormat));

        Tree<Long> tree3 = tree.getNode(3L);
        List<CharSequence> name = tree3.getParentsName(6L, true);
        System.out.println(JSON.toJSONString(name, SerializerFeature.PrettyFormat));
    }
}
