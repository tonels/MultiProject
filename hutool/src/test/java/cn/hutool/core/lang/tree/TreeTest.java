package cn.hutool.core.lang.tree;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用树测试
 *
 * @author liangbaikai
 */
public class TreeTest {
	// 模拟数据
	static List<TreeNode<String>> nodeList = CollUtil.newArrayList();

	static {
		// 模拟数据
		nodeList.add(new TreeNode<>("1", "0", "系统管理父", 0));
		nodeList.add(new TreeNode<>("11", "1", "用户管理", 222222));
		nodeList.add(new TreeNode<>("111", "11", "用户添加", 0));

		nodeList.add(new TreeNode<>("2", "0", "商品管理父", 8));
		nodeList.add(new TreeNode<>("21", "2", "商品管理", 1));
		nodeList.add(new TreeNode<>("221", "21", "商品管理2", 2));
		nodeList.add(new TreeNode<>("2221", "221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("22221", "2221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("222221", "22221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("2222221", "222221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("22222221", "2222221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("222222221", "22222221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("2222222221", "222222221", "商品管理2", 2));
		nodeList.add(new TreeNode<>("22222222221", "2222222221", "商品管理2", 2));

		nodeList.add(new TreeNode<>("3", "0", "店铺管理父", 9));
	}

	/**
	 * 构建Tree
	 * 权重-可以定义返回tree节点排序
	 */
	@Test
	public void buildTree() {
		List<Tree<String>> treeList = TreeUtil.build(nodeList, "0");
		for (Tree<String> tree : treeList) {
			Assert.assertNotNull(tree);
			Assert.assertEquals("0", tree.getParentId());
		}
	}

	/**
	 * 通过当前子节点查找一级父节点
	 */
	@Test
	public void parent() {
		List<Tree<String>> treeList = TreeUtil.build(nodeList, "0");

		// 测试通过子节点查找父节点
		final Tree<String> rootNode0 = treeList.get(0);
		final Tree<String> parent = rootNode0.getChildren().get(0).getParent();
		Assert.assertEquals(rootNode0, parent);
	}

	@Test
	public void parent2() {
		List<Tree<String>> treeList = TreeUtil.build(nodeList, "0");

		// 测试通过子节点查找父节点
		final Tree<String> rootNode0 = treeList.get(0);
		final Tree<String> parent = rootNode0.getChildren().get(0).getParent();
		Tree<String> parent1 = parent.getParent(); // null

		Assert.assertEquals(rootNode0, parent);
	}

	/**
	 * 可自定义属性名
	 */
	@Test
	public void tree() {
		//配置
		TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
		// 自定义属性名 都要默认值的
		treeNodeConfig.setWeightKey("order");
		treeNodeConfig.setIdKey("id");
//		treeNodeConfig.setDeep(); // todo  递归深度,可设置遍历树的深度，用于展示基层节点（注意：节点展开层数过多，会加深堆栈消耗）

		//转换器
		List<Tree<String>> treeNodes = TreeUtil.build(nodeList, "0", treeNodeConfig,
				(treeNode, tree) -> {
					tree.setId(treeNode.getId());
					tree.setParentId(treeNode.getParentId());
					tree.setWeight(treeNode.getWeight());
					tree.setName(treeNode.getName());
					// 扩展属性 ...
					tree.putExtra("extraField", 666);
					tree.putExtra("other", new Object());
				});

		System.out.println(JSON.toJSONString(treeNodes, SerializerFeature.PrettyFormat));
	}

	String  aaa = "";

	@Test
	public void parent3() {
		TreeNode<String> node = new TreeNode<>("111", "11", "用户添加", 0);
		StringBuilder nnnn = new StringBuilder();
		String p = getP3(node);
		System.out.println(p);

//		List<String> list = Lists.newArrayList();
//		List<String> p2 = getP2(list, node);
//		System.out.println(p2.size());
	}

	public String getP(StringBuilder nameBuilder,TreeNode<String> node){

		StringBuilder fullNametotal = new StringBuilder(node.getName()).append("/").append(nameBuilder);

		String parentId = node.getParentId();
		TreeNode<String> stringTreeNode = nodeList.stream().filter(info -> info.getId().equals(parentId)).findFirst().orElse(null);

		if (null != stringTreeNode && !"1".equals(stringTreeNode.getId())) {
			getP(fullNametotal, stringTreeNode);
		}
		return fullNametotal.toString();
	}

	public String getP3(TreeNode<String> node){

		aaa = node.getName()  + "/" + aaa;

		String parentId = node.getParentId();
		TreeNode<String> stringTreeNode = nodeList.stream().filter(info -> info.getId().equals(parentId)).findFirst().orElse(null);

		if (null != stringTreeNode) {
			getP3(stringTreeNode);
		}
		return aaa;
	}

	public List<String> getP2(List<String> strings,TreeNode<String> node){

//		StringBuilder fullNametotal = new StringBuilder(node.getName()).append("/").append(nameBuilder);

		strings.add(node.getName().toString());

		String parentId = node.getParentId();
		TreeNode<String> stringTreeNode = nodeList.stream().filter(info -> info.getId().equals(parentId)).findFirst().orElse(null);

		if (null != stringTreeNode && !"1".equals(stringTreeNode.getId())) {
			getP2(strings, stringTreeNode);
		}
		return strings;
	}


}
