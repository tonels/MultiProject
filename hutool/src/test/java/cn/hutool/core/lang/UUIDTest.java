package cn.hutool.core.lang;

import cn.hutool.core.collection.ConcurrentHashSet;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Strings;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

public class UUIDTest {

	/**
	 * 测试UUID是否存在重复问题
	 */
	@Test
	public void fastUUIDTest(){
		Set<String> set = new ConcurrentHashSet<>(100);
		ThreadUtil.concurrencyTest(100, ()-> set.add(UUID.fastUUID().toString()));
		Assert.assertEquals(100, set.size());
	}

	@Test
	public void uuid_32(){

		System.out.println("java.util.UUID.randomUUID() = " + java.util.UUID.randomUUID());
		System.out.println("UUID.randomUUID() = " + UUID.randomUUID());
		System.out.println("IdUtil.createSnowflake(System.currentTimeMillis(),1L) = " + IdUtil.createSnowflake(1L, 1L).nextId());


//		StrUtil.format(UUID.randomUUID(),'q')
		System.out.println("UUID.randomUUID().toString().replace(\"-\",\"\") = " + UUID.randomUUID().toString().replace("-", ""));


	}

}
