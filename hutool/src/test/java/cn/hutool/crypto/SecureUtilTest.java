package cn.hutool.crypto;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.json.JSONUtil;
import org.junit.Test;

public class SecureUtilTest {

// -------------------------------------------------------- 以下是对称加密 -------------------------------------------------------------

    /**
     * AES加密、解密
     */
    @Test
    public void aes() {
        String content = "test中文";
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        //构建
        AES aes = SecureUtil.aes(key);
        //加密
        byte[] encrypt = aes.encrypt(content);
        //解密
        System.out.println("aes.decryptStr(encrypt, CharsetUtil.CHARSET_UTF_8) = " + aes.decryptStr(encrypt, CharsetUtil.CHARSET_UTF_8));
        //加密为16进制表示
        String encryptHex = aes.encryptHex(content);
        //解密为原字符串
        System.out.println("aes.decryptStr(encryptHex) = " + aes.decryptStr(encryptHex));
    }


    @Test
    public void aes2() {
        String text = "梁帅";

// key：AES模式下，key必须为16位
        String key = "1234567890000000";
// iv：偏移量，ECB模式不需要，CBC模式下必须为16位
        String iv = "1234567890000000";
        AES aes = new AES(Mode.CBC, Padding.ZeroPadding, key.getBytes(), iv.getBytes());

// 加密并进行Base转码
        String encrypt = aes.encryptHex(text);
        System.out.println(encrypt);

// 解密为字符串
        String decrypt = aes.decryptStr(encrypt);
        System.out.println(decrypt);
    }

    /**
     * DES加密、解密
     */
    @Test
    public void des() {
        String content = "test中文";
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue()).getEncoded();
        //构建
        DES des = SecureUtil.des(key);
        //加密解密
        byte[] encrypt = des.encrypt(content);
        System.out.println("des.decryptStr(encrypt,CharsetUtil.CHARSET_UTF_8) = " + des.decryptStr(encrypt, CharsetUtil.CHARSET_UTF_8));
        //加密为16进制，解密为原字符串
        String encryptHex = des.encryptHex(content);
        System.out.println("des.decryptStr(encryptHex) = " + des.decryptStr(encryptHex));
    }

// -------------------------------------------------------- 以下是非对称加密 -------------------------------------------------------------

    /**
     * RSA
     */
    @Test
    public void rsa() {
        RSA rsa = new RSA();
        System.out.println("JSON.toJSONString(rsa) = " + JSONUtil.formatJsonStr(JSONUtil.toJsonStr(rsa)));

        // 这里
        // 加密场景：公钥加密，私钥解密
        byte[] encrypt = rsa.encrypt(StrUtil.bytes("我是一段测试aaaa", CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
        byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
        System.out.println("StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8) = " + StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8));
        // 签名场景：私钥加密，公钥解密
        byte[] encrypt2 = rsa.encrypt(StrUtil.bytes("我是一段测试aaaa", CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
        byte[] decrypt2 = rsa.decrypt(encrypt2, KeyType.PublicKey);
        System.out.println("StrUtil.str(decrypt2, CharsetUtil.CHARSET_UTF_8) = " + StrUtil.str(decrypt2, CharsetUtil.CHARSET_UTF_8));

//        // 还可以hash或者16进制
//        String aa123456 = rsa.encryptHex("Aa123456", KeyType.PublicKey);
//        System.out.println("rsa.encryptHex(\"Aa123456\",KeyType.PublicKey) = " + aa123456);
//        RSA rsa1 = new RSA();
//        String s = rsa1.decryptStr(aa123456, KeyType.PrivateKey, CharsetUtil.CHARSET_UTF_8);
//        System.out.println("解密后 = " + s);


    }

    /**
     * 公钥和私钥是成对的，且生成不是唯一的
     */
    @Test
    public void rsa3() {
        RSA rsa = SecureUtil.rsa();
        String privateKeyBase64 = rsa.getPrivateKeyBase64();
        String publicKeyBase64 = rsa.getPublicKeyBase64();
        System.out.println("私钥 = " + privateKeyBase64);
        System.out.println("公钥 = " + publicKeyBase64);
    }

    /**
     * 取一组密钥对
     * 这里要确保前后端加密,解密算法的一致性
     */
    @Test
    public void rsa4() {
        String priKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMk8Ly/KW0tBiQCxh3LCSVorzufytKKOLEBp4kpWBYIzyyuVya8r1vk5rPcH44m" +
                "TRZvOQ85vMy6n4wczJE7Im9L+EpRNUcXgo6hHe4YDIFw+ADULIPaGdKbdygBIdopEBGI17Vteg6oLjPaS8BvDfwWn/wJigKr0duUQYl2R6rBnAgMBAAECgYASVVFXNpr" +
                "/+DCAcla3YHaeUxoT4ktio+E+3RFTWb2WvHE+FJ9ok23lDBlzSAdgI/KrhyyA73s855QKC+FllrvbYQco4NW08WYp3P41tjOwFSjeQqsirfokjQDQnWQL3BzcACKt8nWK" +
                "Suc2iUTqU9IASjelLr0QCIbLxHlkpi0IgQJBAO6aA4pBVkshd7lHGs4GONNWeyqhe44LIK0Edsy7qQ2zw5LZXZ+ZOnyR2AoRnB0TOo7ghKKa3XpAMMmoMV890/8CQQDX6KXqHRu" +
                "y/RN+McmTJCjBD5FdSzpSiA7cC/TO4bNbNwDd514j1wYBteJCLOTOe2VFmVCKooLN9Vkfvu03hQOZAkEAg4Cu4swP22ieq6bcxx/2wcOwLlbB2g+X+sOzFjN3gO3KpGzYhNXWvkFDa" +
                "Ik8GWo3o/dqIpRn7LDkMsBYNb3F0QJBAJaqVaVifKLRsdmdGqIgWIQovfLRfTh8k4oAmewIWiWr42H2g54U/a8R4nr1YxGn0EraKvcjYo4gyPm7PURCAPkCQEdAuf0vW7/L7HrVRA2Mr" +
                "qtY9C/skl970YtQViJB5+ASMmC44kY7HOLouCNvyPn0qq6otfroyp3X/U/Z45w3P/U=";

       String pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJPC8vyltLQYkAsYdywklaK87n8rSijixAaeJKVgWCM8srlcmvK9b5Oaz3B+OJk0WbzkPObzMup+" +
                "MHMyROyJvS/hKUTVHF4KOoR3uGAyBcPgA1CyD2hnSm3coASHaKRARiNe1bXoOqC4z2kvAbw38Fp/8CYoCq9HblEGJdkeqwZwIDAQAB";

        RSA rsa = SecureUtil.rsa(priKey,pubKey);
        String aa123456 = rsa.encryptBase64("Aa123456", KeyType.PublicKey);
        System.out.println("公钥加密后 = " + aa123456);

        String s = rsa.decryptStr(aa123456, KeyType.PrivateKey, CharsetUtil.CHARSET_UTF_8);
        System.out.println("私钥解密后 = " + s);
    }

}
