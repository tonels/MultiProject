package cn.hutool.json.bean;

import lombok.Data;

@Data
public class KeyBean{
	private String akey;
	private String bkey;
}
