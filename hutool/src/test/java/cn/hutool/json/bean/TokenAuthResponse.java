package cn.hutool.json.bean;

import lombok.Data;

@Data
public class TokenAuthResponse {
	private String token;
	private String userId;
}
