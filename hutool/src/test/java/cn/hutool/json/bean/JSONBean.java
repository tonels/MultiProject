package cn.hutool.json.bean;

import cn.hutool.json.JSONObject;
import lombok.Data;

@Data
public class JSONBean {
	private int code;
	private JSONObject data;
}
